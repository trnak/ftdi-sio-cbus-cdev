# FTDI SIO CBUS GPIO CDEV


## FTDI CBUS GPIO patchset by Stefan Agner 

### modified to provide character device (cdev) interface instead of GPIO interface

[[PATCH 0/2] FTDI CBUS GPIO support](https://lkml.org/lkml/2015/6/20/205)

[[PATCH 1/2] USB: ftdi_sio: add CBUS mode for FT232R devices](https://lkml.org/lkml/2015/6/20/206)

[[PATCH 2/2] gpio: gpio-ftdi-cbus: add driver for FTDI CBUS GPIOs](https://lkml.org/lkml/2015/6/20/207)


## Changes
 * Add support fot FT-X chips.
 * Provide character device /dev/ftdi-cbus-cdevN instead of GPIO interface.
 
